exports["mobxDataModel"] =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! exports provided: Attribute, Model, Structure, Collection, watch$ */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Attribute\", function() { return Attribute; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Model\", function() { return Model; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Structure\", function() { return Structure; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Collection\", function() { return Collection; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"watch$\", function() { return watch$; });\n/* harmony import */ var mobx__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! mobx */ \"mobx\");\n/* harmony import */ var mobx__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(mobx__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ \"rxjs\");\n/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(rxjs__WEBPACK_IMPORTED_MODULE_1__);\n\n\n\nclass Attribute {\n    constructor({defaultValue, initialValue, singleSource = 'first'} = {}) {\n        Object(mobx__WEBPACK_IMPORTED_MODULE_0__[\"decorate\"])(this, {\n            _values: mobx__WEBPACK_IMPORTED_MODULE_0__[\"observable\"].shallow,\n            _singleValue: mobx__WEBPACK_IMPORTED_MODULE_0__[\"observable\"].ref,\n            _isDestroyed: mobx__WEBPACK_IMPORTED_MODULE_0__[\"observable\"].ref\n        });\n        this._values = initialValue ? [initialValue] : [];\n        this._singleValue = initialValue || null;\n        this._isSingleFirst = singleSource === 'first';\n        this._isDestroyed = false;\n        this._defaultValue = defaultValue;\n        this._updateSingleValue();\n    }\n\n    _updateSingleValue() {\n        this._singleValue = this._values.length > 0\n            ? (this._isSingleFirst ? this._values[0].value : this._values[this._values.length - 1])\n            : this._defaultValue;\n    }\n\n    _removeValues(keys) {\n        this._values = this._values.filter(\n            item => !keys.every(key => item.keys.includes(key))\n        );\n        this._updateSingleValue();\n    }\n\n    _addValue(value, keys) {\n        this._values.push({keys, value});\n        if (this._values.length === 1) {\n            this._updateSingleValue();\n        }\n    }\n\n    _getCollection() {\n        return this._values.length > 0\n            ? this._values.map(item => item.value)\n            : this._defaultValue === undefined ? [] : [this._defaultValue];\n    }\n\n    destroy() {\n        this._isDestroyed = true;\n        this._singleValue = null;\n        this._values = null;\n    }\n\n    get value() {\n        return this._singleValue;\n    }\n\n    get collection() {\n        return this._getCollection();\n    }\n}\n\nclass Model {\n    constructor(observeMode = mobx__WEBPACK_IMPORTED_MODULE_0__[\"observable\"].ref) {\n        Object(mobx__WEBPACK_IMPORTED_MODULE_0__[\"decorate\"])(this, {\n            _value: observeMode,\n            _isDestroyed: mobx__WEBPACK_IMPORTED_MODULE_0__[\"observable\"].ref\n        });\n        this._value = null;\n        this._parent = undefined;\n        this._key = undefined;\n        this._root = undefined;\n        this._rules = [];\n        this._appliedRules = [];\n        this._dependencies = new Map();\n        this._attributes = {};\n        this._impact = new Map();\n        this._assign = Object(mobx__WEBPACK_IMPORTED_MODULE_0__[\"action\"])(this._assign);\n        this.declaration(this._define());\n    }\n\n    _define() {\n        return {\n            attribute: this._defineAttribute.bind(this),\n            dependency: this._defineDependency.bind(this),\n            rule: this._defineRule.bind(this),\n            decorator: this._defineDecorator.bind(this)\n        }\n    }\n\n    get _isUndefined() {\n        return this._value === null;\n    }\n\n    _clear() {\n        this._getChildren().forEach(child => child.destroy());\n        this._value = null;\n    }\n\n    _update(newValue) {\n        this._value = newValue;\n    }\n\n    _share() {\n        return this._value;\n    }\n\n    _getChildren() {\n        return [];\n    }\n\n    _representation() {\n        return this._value;\n    }\n\n    _assign(value) {\n        if (value === undefined || value === null) {\n            this._clear();\n        } else {\n            this._update(value);\n        }\n    }\n\n    _setKey(value) {\n        this._key = value;\n    }\n\n    _setParent(parent, fieldKey) {\n        this._parent = parent;\n        this._key = fieldKey;\n        this._root = parent.root || parent;\n    }\n\n    declaration(define) {\n        /* override it to define a model */\n    }\n\n    destroy() {\n        this._isDestroyed = true;\n        this._unregisterDependency();\n        this._clear();\n        this._parent = undefined;\n        this._key = undefined;\n        this._root = undefined;\n        this._dependencies.clear();\n        this._dependencies = undefined;\n        this._appliedRules.forEach(rule => rule.dispose());\n        this._appliedRules = undefined;\n        this._impact.forEach(attributes => {\n            attributes.forEach(attribute => attribute._removeValues([this]));\n        });\n        this._impact = undefined;\n        Object.values(this._attributes).forEach(attribute => attribute.destroy());\n        this._attributes = undefined;\n        this._value = undefined;\n    }\n\n    _testPath(path, mask) {\n        if (mask.length !== path.length) {\n            return false;\n        }\n        for (let i = 0; i < path.length; i++) {\n            if (mask[i] !== '*' && path[i] !== mask[i]) {\n                return false;\n            }\n        }\n        return true;\n    }\n\n    _getPath() {\n        const path = this.key ? [this.key] : [];\n        let parent = this._parent;\n        while (parent) {\n            path.unshift(parent.key);\n            parent = parent._parent;\n        }\n        return path;\n    }\n\n    _getRules(path = []) {\n        let rules = this._rules.filter(rule => this._testPath(path, rule.mask));\n        if (this._parent) {\n            rules = rules.concat(this._parent._getRules([this._key, ...path]));\n        }\n        return rules;\n    }\n\n    _isRuleApplied(rule) {\n        return !!this._appliedRules.find(applied => applied.rule === rule)\n    }\n\n    _applyRules(rules = this._getRules()) {\n        rules.filter(rule => !this._isRuleApplied(rule))\n            .forEach(rule => {\n                const ruleChecker = () => rule.checker(this, rule.holder, rule.mask);\n                const ruleWatcher = Object(mobx__WEBPACK_IMPORTED_MODULE_0__[\"computed\"])(ruleChecker);\n                const ruleAction = checkResult => {\n                    const runRuleAction = Object(mobx__WEBPACK_IMPORTED_MODULE_0__[\"action\"])(() => rule.action(checkResult, this, rule.holder, rule.mask));\n                    if (checkResult && checkResult.then) {\n                        checkResult.then(runRuleAction);\n                    } else {\n                        runRuleAction();\n                    }\n                };\n                const dispose = ruleWatcher.observe(({newValue}) => ruleAction(newValue));\n                this._appliedRules.push({\n                    rule,\n                    dispose: () => {\n                        dispose();\n                        if (rule.release) {\n                            rule.release(this, rule.holder, rule.mask);\n                        }\n                    }\n                });\n\n                Object(mobx__WEBPACK_IMPORTED_MODULE_0__[\"autorun\"])(init => {\n                    if (!this._isDestroyed) {\n                        ruleAction(ruleWatcher.get());\n                    }\n                    init.dispose();\n                });\n            });\n        this._registerDependency();\n    }\n\n    _registerDependency(path = [], model = this) {\n        this._dependencies.forEach((dependency, rule) => {\n            Object.entries(rule).forEach(([name, mask]) => {\n                if (this._testPath(path, mask)) {\n                    const isMultiMask = mask.includes('*');\n                    if (isMultiMask && !dependency[name].includes(model)) {\n                        dependency[name].push(model);\n                    } else if (!isMultiMask) {\n                        dependency[name] = model;\n                    }\n                }\n            })\n        });\n        if (this._parent) {\n            this._parent._registerDependency([this._key, ...path], model);\n        }\n    }\n\n    _unregisterDependency(model = this) {\n        this._dependencies.forEach((dependency, rule) => {\n            Object.entries(rule).forEach(([name, mask]) => {\n                const isMultiMask = mask.includes('*');\n                if (isMultiMask && dependency[name].includes(model)) {\n                    dependency[name].splice(dependency[name].indexOf(model), 1);\n                } else if (!isMultiMask && dependency[name] === model) {\n                    dependency[name] = null;\n                }\n            })\n        });\n        if (this._parent) {\n            this._parent._unregisterDependency(model);\n        }\n    }\n\n    _defineAttribute(attrName, attrConfig) {\n        this._attributes[attrName] = new Attribute(attrConfig);\n    }\n\n    _defineRule(mask, checker, action, release) {\n        const rule = {holder: this, mask: mask === '.' ? [] : mask.split('.'), checker, action, release};\n        this._rules.push(rule);\n        if (mask === '.') {\n            this._applyRules([rule]);\n\n        }\n    }\n\n    _defineDecorator(mask, decoratorFn) {\n        const decorator = (...args) => decoratorFn(...args);\n        this._defineRule(mask, decorator, (decoration, setter) => {\n            const attributes = setter._impact.get(decorator);\n            if (attributes) {\n                attributes.forEach(attribute => attribute._removeValues([setter, decorator]));\n            }\n            if (decoration) {\n                let impact = [];\n                decoration.forEach(([model, attributes]) => {\n                    Object.keys(attributes).forEach(attributeName => {\n                        const attribute = model._attributes[attributeName];\n                        if (!attribute) {\n                            throw Error(`Model [${model._getPath().join('.') || 'root'}] has no attribute [${attributeName}]`);\n                        }\n                        attribute._addValue(attributes[attributeName], [setter, decorator]);\n                        impact.push(attribute);\n                    });\n                });\n                setter._impact.set(decorator, impact);\n            } else {\n                setter._impact.delete(decorator);\n            }\n        });\n    };\n\n    _defineDependency(dependency, ruleChecker, ruleAction) {\n        let dependencyObject = Object(mobx__WEBPACK_IMPORTED_MODULE_0__[\"observable\"])({}, {deep: false});\n        let dependencyRules = {};\n        Object.entries(dependency).forEach(([name, mask]) => {\n            dependencyRules[name] = dependency[name].split('.');\n            const isMultiMask = mask.includes('*');\n            dependencyObject[name] = isMultiMask ? Object(mobx__WEBPACK_IMPORTED_MODULE_0__[\"observable\"])([], {deep: false}) : null;\n        });\n        this._dependencies.set(dependencyRules, dependencyObject);\n\n        this._defineRule(\n            '.',\n            () => ruleChecker(dependencyObject, this),\n            ruleAction,\n            () => {\n                this._dependencies.delete(dependencyRules);\n                dependencyRules = undefined;\n                dependencyObject = undefined;\n            }\n        );\n    }\n\n    _take(path = [], start = 0) {\n        let index = start;\n        let model = this;\n        while (index < path.length - 1 && model) {\n            const key = path[index];\n            if (key === '*') {\n                return model._getChildren()\n                    .map(model => model._take(path, index + 1))\n                    .flat();\n            } else {\n                model = model.get(key);\n                index++;\n            }\n        }\n\n        if (model && index < path.length) {\n            const [expression, property] = path[index].split('::');\n            if (expression === '*') {\n                return model._getChildren()\n                    .map(model => model._take(property ? [`::${property}`] : []))\n                    .flat();\n            } else {\n                const [key, attr] = expression.split('@');\n                model = key === '' ? model : model.get(key);\n                let value = model && attr ? model.attr(attr) : model;\n                return property && value ? value[property] : value;\n            }\n        } else {\n            return model;\n        }\n\n    }\n\n    take(path) {\n        return this._take(path ? path.split('.') : []);\n    }\n\n    get() {\n        return this._isUndefined || arguments.length > 0 ? null : this._value;\n    }\n\n    get value() {\n        return this._representation();\n    }\n\n    set value(newValue) {\n        this._assign(newValue);\n    }\n\n    attr(name) {\n        return this._attributes[name];\n    }\n\n    get parent() {\n        return this._parent;\n    }\n\n    get key() {\n        return this._key;\n    }\n\n    get root() {\n        return this._root;\n    }\n\n    static declare(declarationFn) {\n        return class extends Model {\n            declaration(define) {\n                declarationFn.call(this, define);\n            }\n        }\n    }\n}\n\nclass Structure extends Model {\n    constructor(structure) {\n        super(mobx__WEBPACK_IMPORTED_MODULE_0__[\"observable\"].shallow);\n        this._structure = structure || this._structure;\n        this._fieldNames = Object.getOwnPropertyNames(this._structure);\n        this._representationObject = {};\n        const representationOf = fieldName => () => this._value[fieldName]._representation();\n        this._fieldNames.forEach(fildName => {\n            Object.defineProperty(this._representationObject, fildName, {\n                get: representationOf(fildName),\n                enumerable: true\n            });\n        });\n    }\n\n    _define() {\n        return {\n            ...super._define(),\n            structure: structure => this._structure = structure\n        }\n    }\n\n    _init() {\n        this._value = {};\n        this._fieldNames\n            .forEach(fieldName => {\n                this._value[fieldName] = new this._structure[fieldName]();\n                this._value[fieldName]._setParent(this, fieldName);\n                this._value[fieldName]._applyRules();\n            });\n    }\n\n    _update(newValue) {\n        this._clear();\n        this._init();\n        this._fieldNames\n            .forEach(fieldName => {\n                this._value[fieldName]._assign(newValue[fieldName]);\n            });\n    }\n\n    _share() {\n        return this._isUndefined ? null : {...this._value};\n    }\n\n    _representation() {\n        return this._isUndefined ? null : this._representationObject;\n    }\n\n    _getChildren() {\n        return this._isUndefined ? [] : this._fieldNames.map(fieldName => this._value[fieldName]);\n    }\n\n    get(name) {\n        if (this._isUndefined) {\n            return null;\n        } else if (arguments.length === 0) {\n            return this._share();\n        } else {\n            return this._value[name];\n        }\n    }\n\n    static of(structure) {\n        return class extends Structure {\n            constructor() {\n                super(structure);\n            }\n        }\n    }\n}\n\nclass Collection extends Model {\n    constructor(type) {\n        super(mobx__WEBPACK_IMPORTED_MODULE_0__[\"observable\"].shallow);\n        this.add = Object(mobx__WEBPACK_IMPORTED_MODULE_0__[\"action\"])(this.add);\n        this.clear_ = Object(mobx__WEBPACK_IMPORTED_MODULE_0__[\"action\"])(this.clear);\n        this.remove = Object(mobx__WEBPACK_IMPORTED_MODULE_0__[\"action\"])(this.remove);\n        this._type = type || this._type;\n    }\n\n    _define() {\n        return {\n            ...super._define(),\n            type: type => this._type = type\n        }\n    }\n\n    _init() {\n        this._value = [];\n    }\n\n    _update(newValue) {\n        this._clear();\n        this._init();\n        newValue.forEach(value => this._add(value));\n    }\n\n    _share() {\n        return this._isUndefined ? null : [...this._value];\n    }\n\n    _representation() {\n        return this._isUndefined ? null : this._value.map(model => model._representation());\n    }\n\n    _getChildren() {\n        return this._isUndefined ? [] : this._value;\n    }\n\n    _add(value) {\n        const item = new this._type();\n        this._value.push(item);\n        item._setParent(this, this._value.length);\n        item._applyRules();\n        item._assign(value);\n        return item;\n    }\n\n    _remove(index) {\n        this._value[index]._destroy();\n        this._value.splice(index, 1);\n        this._value.forEach((model, index) => model._setKey(index));\n    }\n\n    add(value) {\n        if (this._isUndefined) {\n            this._init();\n        }\n        return this._add(value);\n    }\n\n    remove(index) {\n        if (!this._isUndefined) {\n            this._remove(index);\n        }\n    }\n\n    get count() {\n        return this._isUndefined ? 0 : this._value.length;\n    }\n\n    clear() {\n        this._clear();\n        this._init();\n    }\n\n    get(index) {\n        if (this._isUndefined) {\n            return null;\n        } else if (arguments.length === 0) {\n            return this._share();\n        } else {\n            return this._value[index];\n        }\n    }\n\n    static of(dataClass) {\n        return class extends Collection {\n            constructor() {\n                super(dataClass);\n            }\n        };\n    }\n}\n\nfunction watch$(getter, after) {\n    return new rxjs__WEBPACK_IMPORTED_MODULE_1__[\"Observable\"](observer => Object(mobx__WEBPACK_IMPORTED_MODULE_0__[\"autorun\"])(reaction => {\n        try {\n            observer.next(getter());\n        } catch (e) {\n            observer.error(e);\n            reaction.dispose();\n        } finally {\n            after && after();\n        }\n    }));\n}\n\n//# sourceURL=webpack://mobxDataModel/./src/index.js?");

/***/ }),

/***/ "mobx":
/*!***********************!*\
  !*** external "mobx" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"mobx\");\n\n//# sourceURL=webpack://mobxDataModel/external_%22mobx%22?");

/***/ }),

/***/ "rxjs":
/*!***********************!*\
  !*** external "rxjs" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"rxjs\");\n\n//# sourceURL=webpack://mobxDataModel/external_%22rxjs%22?");

/***/ })

/******/ });