import {action, autorun} from "mobx";
import {Model, Structure, Collection} from './index'
// import {Model, Structure, Collection} from '../public/bundle'

class MString extends Model {
    _update(newValue) {
        super._update(String(newValue));
    }

    declaration(define) {
        define.attribute('hint', 'string hint')
    }
}

class EventType extends Model {
    declaration(define) {
        define.attribute('error');
        define.attribute('hint', {defaultValue: 'type hint'});
    }
}

class EventStatus extends Model {

}

// class EventModel extends Structure {
//     constructor() {
//         super({
//             type: EventType,
//             status: EventStatus,
//             subject: MString,
//             category: MString,
//             actions: Collection.of(MString)
//         });
//
//         // this.addAttr('hint', 'Enter value');
//     }
// }


class EventModel extends Structure {
    declaration(define) {
        define.structure({
            type: EventType,
            status: EventStatus,
            subject: MString,
            category: MString,
            actions: Collection.of(MString)
        });
        define.attribute('error');
        define.attribute('hint', {defaultValue: 'event hint'});
    }
};

// class EventMonitorModel extends Structure {
//     constructor() {
//         super({
//             status: MString,
//             type: MString,
//             events: Collection.of(EventModel)
//         })
//     }
// }


const notNullValue = (message) => (model) => {
    if (model.value === null) {
        return [
            [model, {error: message, hint: 'Please, correct the value'}]
        ]
    }
};

const printValue = (model) => {
    console.log('Value of', model.key, ' is "', model.value, '"');
};

const EventMonitorModel = Structure.of({
    status: MString,
    type: MString,
    events: Collection.of(EventModel)
});


class EventMonitor extends Structure {
    declaration(define) {
        define.structure({
            status: MString,
            type: MString,
            events: Collection.of(EventModel)
        });

        define.attribute('error');
        define.attribute('foo');
        define.attribute('hint', {defaultValue: 'the hint of Monitor'});

        define.decorator('events.*.type', notNullValue('Type can not be null'));
        define.decorator('events.*.type', this.printValue);
    }

    printValue(model) {
        console.log('Value of', model.key, ' is "', model.value, '"');
    }
}


// window.EventMonitor = EventMonitorModel;
// window.em = new EventMonitorModel();
window.em = new EventMonitor();


window.action = action;
window.autorun = autorun;

window.em._defineDecorator('events.*.type', type => {
    // console.log('decorator NOT NULL');
    if (type.value === null) {
        return [
            [type, {
                error: `Invalid type value`,
                hint: 'Set correct value'
            }],
            [type.parent, {
                error: `Event has invalid type value`
            }],
            [type.root, {
                error: `Event [${type.parent.key}] has invalid type value`
            }]
        ]
    }
});

// window.em._defineDependency(
//     {types: 'events.*.type', status: 'status'},
//     (dep) => {
//         let {types, status} = dep;
//         let res = [];
//         !status && res.push('status does not exists');
//         !types.length && res.push('types do not exist');
//         types.forEach(type => {
//             !type && res.push('type is null');
//             type && status && type.value === status.value && res.push(`status === type[${type.parent.key}]`);
//             type && status && type.value !== status.value && res.push(`status !== type[${type.parent.key}]`);
//         });
//         return res.join('|');
//     },
//     result => {
//         console.log(result);
//     }
// );

autorun(() => {
    console.log('error', em.attr('error').collection);
});

window.em.value = {
    status: 'ready',
    events: [
        {type: 101, actions: ['foo', 'bar']},
        {type: 102, actions: ['foo', 'bar']},
        {type: 105, actions: ['foo', 'bar']},
        {type: 105, actions: ['foo', 'bar']},
        {type: 106, actions: ['foo', 'bar']}
    ]
};

// [
//     {type: 101},
//     {type: 102},
//     {type: 105},
//     {type: 105},
//     {type: 106}
// ]

window.huge = [];
for (let i = 0; i < 10000; i++) {
    window.huge.push({type: null});
}


class EventMonitor2 extends EventMonitor {
    declaration(define) {
        super.declaration(define);
        define.attribute('caption', {defaultValue: 'The model'});
    }
}

window.em2 = new EventMonitor2();

window.Model = Model;


window.Test = class Test extends Structure {
    declaration(define) {
        super.declaration(define);

        define.structure({
            events: Collection.of(Model),
            name: Model.declare(define => {
                define.attribute('error');
            })
        })
    }
}