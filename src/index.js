import {computed, observable, decorate, action, autorun} from "mobx";
import {Observable} from "rxjs";

export class Attribute {
    constructor(options = {}) {
        const {defaultValue, initialValue, singleSource = 'first'} = options;
        decorate(this, {
            _values: observable.shallow,
            _singleValue: observable.ref,
            _isDestroyed: observable.ref
        });
        this._values = initialValue ? [initialValue] : [];
        this._singleValue = initialValue || null;
        this._isSingleFirst = singleSource === 'first';
        this._isDestroyed = false;
        this._defaultValue = defaultValue;
        this._updateSingleValue();
    }

    _updateSingleValue() {
        this._singleValue = this._values.length > 0
            ? (this._isSingleFirst ? this._values[0].value : this._values[this._values.length - 1])
            : this._defaultValue;
    }

    _removeValues(keys) {
        this._values = this._values.filter(
            item => !keys.every(key => item.keys.includes(key))
        );
        this._updateSingleValue();
    }

    _addValue(value, keys) {
        this._values.push({keys, value});
        if (this._values.length === 1) {
            this._updateSingleValue();
        }
    }

    _getCollection() {
        return this._values.length > 0
            ? this._values.map(item => item.value)
            : this._defaultValue === undefined ? [] : [this._defaultValue];
    }

    destroy() {
        this._isDestroyed = true;
        this._singleValue = null;
        this._values = null;
    }

    get value() {
        return this._singleValue;
    }

    get collection() {
        return this._getCollection();
    }
}

export class Model {
    constructor(observeMode = observable.ref) {
        decorate(this, {
            _value: observeMode,
            _isDestroyed: observable.ref
        });
        this._value = null;
        this._parent = undefined;
        this._key = undefined;
        this._root = undefined;
        this._rules = [];
        this._appliedRules = [];
        this._dependencies = new Map();
        this._attributes = {};
        this._impact = new Map();
        this._assign = action(this._assign);
        this.declaration(this._define());
    }

    _define() {
        return {
            attribute: this._defineAttribute.bind(this),
            dependency: this._defineDependency.bind(this),
            rule: this._defineRule.bind(this),
            decorator: this._defineDecorator.bind(this)
        }
    }

    get _isUndefined() {
        return this._value === null;
    }

    _clear() {
        this._getChildren().forEach(child => child.destroy());
        this._value = null;
    }

    _update(newValue) {
        this._value = newValue;
    }

    _share() {
        return this._value;
    }

    _getChildren() {
        return [];
    }

    _representation() {
        return this._value;
    }

    _assign(value) {
        if (value === undefined || value === null) {
            this._clear();
        } else {
            this._update(value);
        }
    }

    _setKey(value) {
        this._key = value;
    }

    _setParent(parent, fieldKey) {
        this._parent = parent;
        this._key = fieldKey;
        this._root = parent.root || parent;
    }

    declaration(define) {
        /* override it to define a model */
    }

    destroy() {
        this._isDestroyed = true;
        this._unregisterDependency();
        this._clear();
        this._parent = undefined;
        this._key = undefined;
        this._root = undefined;
        this._dependencies.clear();
        this._dependencies = undefined;
        this._appliedRules.forEach(rule => rule.dispose());
        this._appliedRules = undefined;
        this._impact.forEach(attributes => {
            attributes.forEach(attribute => attribute._removeValues([this]));
        });
        this._impact = undefined;
        Object.values(this._attributes).forEach(attribute => attribute.destroy());
        this._attributes = undefined;
        this._value = undefined;
    }

    _testPath(path, mask) {
        if (mask.length !== path.length) {
            return false;
        }
        for (let i = 0; i < path.length; i++) {
            if (mask[i] !== '*' && path[i] !== mask[i]) {
                return false;
            }
        }
        return true;
    }

    _getPath() {
        const path = this.key ? [this.key] : [];
        let parent = this._parent;
        while (parent) {
            path.unshift(parent.key);
            parent = parent._parent;
        }
        return path;
    }

    _getRules(path = []) {
        let rules = this._rules.filter(rule => this._testPath(path, rule.mask));
        if (this._parent) {
            rules = rules.concat(this._parent._getRules([this._key, ...path]));
        }
        return rules;
    }

    _isRuleApplied(rule) {
        return !!this._appliedRules.find(applied => applied.rule === rule)
    }

    _applyRules(rules = this._getRules()) {
        rules.filter(rule => !this._isRuleApplied(rule))
            .forEach(rule => {
                const ruleChecker = () => rule.checker(this, rule.holder, rule.mask);
                const ruleWatcher = computed(ruleChecker);
                const ruleAction = checkResult => {
                    const runRuleAction = action(() => rule.action(checkResult, this, rule.holder, rule.mask));
                    if (checkResult && checkResult.then) {
                        checkResult.then(runRuleAction);
                    } else {
                        runRuleAction();
                    }
                };
                const dispose = ruleWatcher.observe(({newValue}) => ruleAction(newValue));
                this._appliedRules.push({
                    rule,
                    dispose: () => {
                        dispose();
                        if (rule.release) {
                            rule.release(this, rule.holder, rule.mask);
                        }
                    }
                });

                autorun(init => {
                    if (!this._isDestroyed) {
                        ruleAction(ruleWatcher.get());
                    }
                    init.dispose();
                });
            });
        this._registerDependency();
    }

    _registerDependency(path = [], model = this) {
        this._dependencies.forEach((dependency, rule) => {
            Object.entries(rule).forEach(([name, mask]) => {
                if (this._testPath(path, mask)) {
                    const isMultiMask = mask.includes('*');
                    if (isMultiMask && !dependency[name].includes(model)) {
                        dependency[name].push(model);
                    } else if (!isMultiMask) {
                        dependency[name] = model;
                    }
                }
            })
        });
        if (this._parent) {
            this._parent._registerDependency([this._key, ...path], model);
        }
    }

    _unregisterDependency(model = this) {
        this._dependencies.forEach((dependency, rule) => {
            Object.entries(rule).forEach(([name, mask]) => {
                const isMultiMask = mask.includes('*');
                if (isMultiMask && dependency[name].includes(model)) {
                    dependency[name].splice(dependency[name].indexOf(model), 1);
                } else if (!isMultiMask && dependency[name] === model) {
                    dependency[name] = null;
                }
            })
        });
        if (this._parent) {
            this._parent._unregisterDependency(model);
        }
    }

    _defineAttribute(attrName, attrConfig) {
        this._attributes[attrName] = new Attribute(attrConfig);
    }

    _defineRule(mask, checker, action, release) {
        const rule = {holder: this, mask: mask === '.' ? [] : mask.split('.'), checker, action, release};
        this._rules.push(rule);
        if (mask === '.') {
            this._applyRules([rule]);

        }
    }

    _defineDecorator(mask, decoratorFn) {
        const decorator = (...args) => decoratorFn(...args);
        this._defineRule(mask, decorator, (decoration, setter) => {
            const attributes = setter._impact.get(decorator);
            if (attributes) {
                attributes.forEach(attribute => attribute._removeValues([setter, decorator]));
            }
            if (decoration) {
                let impact = [];
                decoration.forEach(([model, attributes]) => {
                    Object.keys(attributes).forEach(attributeName => {
                        const attribute = model._attributes[attributeName];
                        if (!attribute) {
                            throw Error(`Model [${model._getPath().join('.') || 'root'}] has no attribute [${attributeName}]`);
                        }
                        attribute._addValue(attributes[attributeName], [setter, decorator]);
                        impact.push(attribute);
                    });
                });
                setter._impact.set(decorator, impact);
            } else {
                setter._impact.delete(decorator);
            }
        });
    };

    _defineDependency(dependency, ruleChecker, ruleAction) {
        let dependencyObject = observable({}, {deep: false});
        let dependencyRules = {};
        Object.entries(dependency).forEach(([name, mask]) => {
            dependencyRules[name] = dependency[name].split('.');
            const isMultiMask = mask.includes('*');
            dependencyObject[name] = isMultiMask ? observable([], {deep: false}) : null;
        });
        this._dependencies.set(dependencyRules, dependencyObject);

        this._defineRule(
            '.',
            () => ruleChecker(dependencyObject, this),
            ruleAction,
            () => {
                this._dependencies.delete(dependencyRules);
                dependencyRules = undefined;
                dependencyObject = undefined;
            }
        );
    }

    _take(path = [], start = 0) {
        let index = start;
        let model = this;
        while (index < path.length - 1 && model) {
            const key = path[index];
            if (key === '*') {
                return model._getChildren()
                    .map(model => model._take(path, index + 1))
                    .flat();
            } else {
                model = model.get(key);
                index++;
            }
        }

        if (model && index < path.length) {
            const [expression, property] = path[index].split('::');
            if (expression === '*') {
                return model._getChildren()
                    .map(model => model._take(property ? [`::${property}`] : []))
                    .flat();
            } else {
                const [key, attr] = expression.split('@');
                model = key === '' ? model : model.get(key);
                let value = model && attr ? model.attr(attr) : model;
                return property && value ? value[property] : value;
            }
        } else {
            return model;
        }

    }

    take(path) {
        return this._take(path ? path.split('.') : []);
    }

    get() {
        return this._isUndefined || arguments.length > 0 ? null : this._value;
    }

    get value() {
        return this._representation();
    }

    set value(newValue) {
        this._assign(newValue);
    }

    attr(name) {
        return this._attributes[name];
    }

    get parent() {
        return this._parent;
    }

    get key() {
        return this._key;
    }

    get root() {
        return this._root;
    }

    static declare(declarationFn) {
        return class extends Model {
            declaration(define) {
                declarationFn.call(this, define);
            }
        }
    }
}

export class Structure extends Model {
    constructor(structure) {
        super(observable.shallow);
        this._structure = structure || this._structure;
        this._fieldNames = Object.getOwnPropertyNames(this._structure);
        this._representationObject = {};
        const representationOf = fieldName => () => this._value[fieldName]._representation();
        this._fieldNames.forEach(fildName => {
            Object.defineProperty(this._representationObject, fildName, {
                get: representationOf(fildName),
                enumerable: true
            });
        });
    }

    _define() {
        return {
            ...super._define(),
            structure: structure => this._structure = structure
        }
    }

    _init() {
        this._value = {};
        this._fieldNames
            .forEach(fieldName => {
                this._value[fieldName] = new this._structure[fieldName]();
                this._value[fieldName]._setParent(this, fieldName);
                this._value[fieldName]._applyRules();
            });
    }

    _update(newValue) {
        this._clear();
        this._init();
        this._fieldNames
            .forEach(fieldName => {
                this._value[fieldName]._assign(newValue[fieldName]);
            });
    }

    _share() {
        return this._isUndefined ? null : {...this._value};
    }

    _representation() {
        return this._isUndefined ? null : this._representationObject;
    }

    _getChildren() {
        return this._isUndefined ? [] : this._fieldNames.map(fieldName => this._value[fieldName]);
    }

    get(name) {
        if (this._isUndefined) {
            return null;
        } else if (arguments.length === 0) {
            return this._share();
        } else {
            return this._value[name];
        }
    }

    static of(structure) {
        return class extends Structure {
            constructor() {
                super(structure);
            }
        }
    }
}

export class Collection extends Model {
    constructor(type) {
        super(observable.shallow);
        this.add = action(this.add);
        this.clear_ = action(this.clear);
        this.remove = action(this.remove);
        this._type = type || this._type;
    }

    _define() {
        return {
            ...super._define(),
            type: type => this._type = type
        }
    }

    _init() {
        this._value = [];
    }

    _update(newValue) {
        this._clear();
        this._init();
        newValue.forEach(value => this._add(value));
    }

    _share() {
        return this._isUndefined ? null : [...this._value];
    }

    _representation() {
        return this._isUndefined ? null : this._value.map(model => model._representation());
    }

    _getChildren() {
        return this._isUndefined ? [] : this._value;
    }

    _add(value) {
        const item = new this._type();
        this._value.push(item);
        item._setParent(this, this._value.length);
        item._applyRules();
        item._assign(value);
        return item;
    }

    _remove(index) {
        this._value[index]._destroy();
        this._value.splice(index, 1);
        this._value.forEach((model, index) => model._setKey(index));
    }

    add(value) {
        if (this._isUndefined) {
            this._init();
        }
        return this._add(value);
    }

    remove(index) {
        if (!this._isUndefined) {
            this._remove(index);
        }
    }

    get count() {
        return this._isUndefined ? 0 : this._value.length;
    }

    clear() {
        this._clear();
        this._init();
    }

    get(index) {
        if (this._isUndefined) {
            return null;
        } else if (arguments.length === 0) {
            return this._share();
        } else {
            return this._value[index];
        }
    }

    static of(dataClass) {
        return class extends Collection {
            constructor() {
                super(dataClass);
            }
        };
    }
}

export function watch$(getter, after) {
    return new Observable(observer => autorun(reaction => {
        try {
            observer.next(getter());
        } catch (e) {
            observer.error(e);
            reaction.dispose();
        } finally {
            after && after();
        }
    }));
}