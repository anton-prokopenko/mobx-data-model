var webpack = require('webpack');

module.exports = {
    entry: './src/index.js',
    output: {
        path: __dirname + '/public',
        filename: 'bundle.js',
        library: 'mobxDataModel',
        libraryTarget: 'commonjs'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                // loader: 'babel-loader',
                exclude: /node_modules/,
            }
        ]
    },
    optimization: {
        minimize: false
    },
    externals: {
        mobx: 'mobx',
        rxjs: 'rxjs'
    }
};